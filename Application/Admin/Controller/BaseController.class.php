<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP3.2混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Controller;


use Admin\Model\UserModel;
use Admin\Service\UserPermissionService;
use Think\Controller;

class BaseController extends Controller
{
    // 用户ID
    protected $userId;
    // 用户信息
    protected $userInfo;
    // 模型
    protected $model;
    // 服务
    protected $service;

    /**
     * 初始化
     * @author 牧羊人
     * @since 2021/1/17
     */
    protected function _initialize()
    {
        // 初始化配置
        $this->initConfig();
        // 初始化登录
        $this->initLogin();
    }

    /**
     * 初始化配置
     * @author 牧羊人
     * @since 2021/1/17
     */
    private function initConfig()
    {
        // 网站全称
        $this->assign("siteName", C('SITE_NAME'));
        // 网站简称
        $this->assign("nickName", C('NICK_NAME'));
        // 版本号
        $this->assign('version', C('VERSION'));

        //系统分页参数
        define('PERPAGE', isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 0);
        define('PAGE', isset($_REQUEST['page']) ? $_REQUEST['page'] : 0);

        // 数据表前缀
        define('DB_PREFIX', C('DB_PREFIX'));
        // 数据库名
        define('DB_NAME', C('DB_NAME'));

        //系统应用参数
        define('APP', CONTROLLER_NAME);
        define('ACT', ACTION_NAME);
        $this->assign('module', __MODULE__);
        $this->assign('app', APP);
        $this->assign('act', ACT);
    }

    /**
     * 初始化登录
     * @return void
     */
    private function initLogin()
    {
        // 未登录跳转至登录页
        $noLoginActs = array("Login");
        if (!isset($_SESSION['userId']) && !in_array(CONTROLLER_NAME, $noLoginActs)) {
            $this->redirect('/Login/index');
            exit;
        }

        // 登录用户ID
        $userId = session('userId');
        if (!empty($userId)) {
            $this->userId = $userId;

            // 登录用户信息
            $userModel = new UserModel();
            $userInfo = $userModel->getInfo($userId);
            $this->userInfo = $userInfo;
            $this->assign("userId", $this->userId);
            $this->assign("userInfo", $this->userInfo);

            // 获取权限列表
            $userPermissionService = new UserPermissionService();
            $permissionList = $userPermissionService->getPermissionFuncList($userId);
            $this->assign("permissionList", $permissionList);
        }
    }

    /**
     * 后台入口
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function index()
    {
        if (IS_POST) {
            $result = $this->service->getList();
            $this->ajaxReturn($result);
            return;
        }
        // 默认参数
        $param = func_get_args();
        if (!empty($param)) {
            foreach ($param[0] as $key => $val) {
                $this->assign($key, $val);
            }
        }
        $this->render();
    }

    /**
     * 添加或编辑
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function edit()
    {
        if (IS_POST) {
            $result = $this->service->edit();
            $this->ajaxReturn($result);
            return;
        }
        $id = I("get.id", 0);
        if ($id) {
            $info = $this->model->getInfo($id);
        } else {
            // 默认参数
            $param = func_get_args();
            if (!empty($param)) {
                foreach ($param[0] as $key => $val) {
                    $info[$key] = $val;
                }
            }
        }
        $this->assign('info', $info);
        $this->render();
    }

    /**
     * 记录详情
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function detail()
    {
        if (IS_POST) {
            $result = $this->service->edit();
            $this->ajaxReturn($result);
            return;
        }
        $id = I("get.id", 0);
        if ($id) {
            $info = $this->model->getInfo($id);
            $this->assign('info', $info);
        }
        $this->render();
    }

    /**
     * 删除记录
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function drop()
    {
        if (IS_POST) {
            $id = I('post.id');
            $info = $this->model->getInfo($id);
            if ($info) {
                $result = $this->model->drop($id);
                if ($result) {
                    $this->ajaxReturn(message());
                    return;
                }
            }
            $this->ajaxReturn(message($this->model->getError(), false));
        }
    }

    /**
     * 批量删除
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function batchDrop()
    {
        if (IS_POST) {
            $ids = explode(',', $_POST['id']);
            // 删除
            $num = 0;
            foreach ($ids as $key => $val) {
                $res = $this->model->delete($val);
                if ($res !== false) {
                    $num++;
                }
            }
            $this->ajaxReturn(message('本次共选择' . count($ids) . "个条数据,删除" . $num . "个"));
        }
    }

    /**
     * 渲染模板
     * @param string $tpl 模板路径
     * @param array $param 参数
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function render($tpl = "", $param = array())
    {
        if (empty($tpl)) {
            $tpl = ACT;
        } else if (strpos($tpl, ".html") > 0) {
            $tpl = rtrim($tpl, ".html");
        }
        // 参数绑定
        if (!empty($param)) {
            foreach ($param as $name => $val) {
                $this->assign($name, $val);
            }
        }

        // 渲染头部
        $this->display("Public:header");
        // 渲染主体
        if (strpos($tpl, "/") === false) {
            $tpl = ltrim($tpl, "/");
        }
        $this->display(APP . ":{$tpl}");
        // 渲染底部
        $this->display("Public:footer");
    }

    /**
     * 404错误页面
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function _empty()
    {
        $this->display('Public:404');
    }

    /**
     * 设置状态
     * @return mixed
     * @since 2021/1/18
     * @author 牧羊人
     */
    public function setStatus()
    {
        if (IS_POST) {
            $result = $this->service->setStatus();
            $this->ajaxReturn($result);
            return;
        }
    }

}