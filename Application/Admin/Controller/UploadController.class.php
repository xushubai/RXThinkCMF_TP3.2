<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP3.2混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Controller;


use Think\Upload;

class UploadController extends BaseController
{
    /**
     * 上传文件
     * @author 牧羊人
     * @since 2021/3/19
     */
    public function uploadImage()
    {
        $upload = new Upload();// 实例化上传类
        $upload->maxSize = 10 * 1024 * 1024;// 设置附件上传大小10MB
        $upload->exts = explode('|', "jpg|png|gif|bmp|jpeg");// 设置附件上传类型
        $upload->rootPath = ATTACHMENT_PATH; // 设置附件上传根目录
        $upload->savePath = '/temp/';
        $upload->subName = "";
        $info = $upload->uploadOne($_FILES['file']);
        if (!$info) {
            // 上传错误提示错误信息
            //$this->error($upload->getError());
            $this->ajaxReturn(message($upload->getError(), false));
        } else {
            // 上传成功 获取上传文件信息
            $filePath = $info['savepath'] . $info['savename'];
            if (strpos($filePath, IMG_URL) === FALSE) {
                $filePath = IMG_URL . $filePath;
            }
            $this->ajaxReturn(message('上传成功', true, $filePath));
        }
    }
}