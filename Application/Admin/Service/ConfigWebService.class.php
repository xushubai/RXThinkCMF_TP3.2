<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP3.2混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Service;

use Admin\Model\ConfigDataModel;

/**
 * 系统配置-服务类
 * @author 牧羊人
 * @since 2022/2/13
 */
class ConfigWebService extends BaseService
{
    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->model = new ConfigDataModel();
    }

    /**
     * 提交配置信息
     * @return array
     * @since 2022/2/13
     * @author 牧羊人
     */
    public function config()
    {
        // 参数
        $data = I('post.', '', 'trim');
        if (empty($data)) {
            return message("数据不能为空", false);
        }
        foreach ($data as $key => $val) {
            if (strpos($key, 'checkbox')) {
                $item = explode('__', $key);
                $key = $item[0];
                $val = implode(',', array_keys($val));
            } elseif (strpos($key, 'upimage')) {
                $item = explode('__', $key);
                $key = $item[0];
                if (strpos($val, "temp") !== false) {
                    //新上传图片
                    $val = save_image($val, 'config');
                } else {
                    $val = str_replace(IMG_URL, "", $val);
                }
            } elseif (strpos($key, 'upimgs')) {
                $item = explode('__', $key);
                $key = $item[0];

                $imgArr = explode(',', $val);
                $imgStr = [];
                foreach ($imgArr as $kt => $vt) {
                    if (strpos($vt, "temp")) {
                        //新上传图片
                        $imgStr[] = save_image($vt, 'config');
                    } else {
                        //过滤已上传图片
                        $imgStr[] = str_replace(IMG_URL, "", $vt);
                    }
                }
                $val = serialize($imgStr);
            } elseif (strpos($key, 'ueditor')) {
                $item = explode('__', $key);
                $key = $item[0];
                //内容处理
                save_image_content($val, '', "config");
            }
            $info = $this->model->getInfoByAttr([
                ['name' => $key],
            ]);
            if (!$info) {
                continue;
            }
            $this->model->edit([
                'id' => $info['id'],
                'value' => $val,
            ]);
        }
        return message();
    }
}