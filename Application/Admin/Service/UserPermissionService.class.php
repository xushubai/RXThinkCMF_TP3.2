<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP3.2混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Service;

use Admin\Model\MenuModel;
use Admin\Model\UserModel;
use Admin\Model\UserPermissionModel;

/**
 * 用户权限-服务类
 * @author 牧羊人
 * @since 2021/3/14
 * Class UserPermissionService
 * @package Admin\Service
 */
class UserPermissionService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2021/3/14
     * UserPermissionService constructor.
     */
    public function __construct()
    {
        $this->model = new UserPermissionModel();
    }

    /**
     * 获取权限菜单列表
     * @return array
     * @since 2021/3/14
     * @author 牧羊人
     */
    public function getPermissionList()
    {
        // 获取参数
        $param = I("request.");
        // 类型
        $type = (int)$param['type'];
        // 类型值
        $typeId = (int)$param['typeId'];
        // 获取菜单列表
        $menuModel = new MenuModel();
        $menuList = $menuModel->where("mark=1")->order("sort asc")->select();

        // 获取已拥有的权限
        $where = array();
        $where['type'] = $type;
        $where['type_id'] = $typeId;
        $userPermissionModel = new UserPermissionModel();
        $userPermissionList = $userPermissionModel->where($where)->field('menu_id')->select();
        $checkList = array();
        if ($userPermissionList) {
            $checkList = array_column($userPermissionList, "menu_id");
        }
        $list = array();
        if (!empty($menuList)) {
            foreach ($menuList as $val) {
                $item = array();
                $item['id'] = $val['id'];
                $item['name'] = trim($val['name']);
                $item['pId'] = $val['pid'];
                if (in_array($val['id'], $checkList)) {
                    $item['checked'] = true;
                } else {
                    $item['checked'] = false;
                }
                $item['open'] = true;
                $list[] = $item;
            }
        }
        return message("操作成功", true, $list);
    }

    /**
     * 设置权限
     * @return array
     * @since 2021/3/14
     * @author 牧羊人
     */
    public function setPermission()
    {
        // 获取参数
        $param = I("request.");
        // 类型
        $type = (int)$param['type'];
        // 类型值
        $typeId = (int)$param['typeId'];
        if (empty($typeId) || empty($typeId)) {
            return message("类型值不能为空", false);
        }

        // 批量删除已存在的数据
        $userPermissionModel = new UserPermissionModel();
        $userPermissionModel->where(array(
            'type' => $type,
            'type_id' => $typeId,
        ))->delete();

        // 权限ID
        $authIds = trim($param['permissionIds']);
        if ($authIds) {
            $itemArr = explode(',', $authIds);
            foreach ($itemArr as $val) {
                $data = array(
                    'type' => $type,
                    'type_id' => $typeId,
                    'menu_id' => $val,
                );
                $userPermissionModel->add($data);
            }
        }
        return message();
    }

    /**
     * 获取权限菜单列表
     * @param $userId 用户ID
     * @return array|bool|mixed|string|null
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function getPermissionMenuList($userId)
    {
        if ($userId == 1) {
            // 管理员(拥有全部权限)
            $menuModel = new MenuModel();
            $menuList = $menuModel->getChilds(0);
            return $menuList;
        } else {
            // 非管理员
            $userModel = new UserModel();
            $userInfo = $userModel->getInfo($userId);
            $menuList = $this->model->getPermissionMenuList($userInfo['role_ids'], $userId, 1, 0);
            return $menuList;
        }
    }

    /**
     * 获取权限节点列表
     * @param $userId 用户ID
     * @return array
     * @author 牧羊人
     * @since 2022/2/16
     */
    public function getPermissionFuncList($userId)
    {
        if ($userId == 1) {
            // 管理员
            $menuModel = new MenuModel();
            $menuList = $menuModel->where([
                'type' => 4,
                'mark' => 1,
            ])->field("permission")->select();
            $permissionList = array_key_value($menuList, "permission");
            return $permissionList;
        } else {
            // 非管理员
            $userModel = new UserModel();
            $userInfo = $userModel->getInfo($userId);
            $permissionList = $this->model->getPermissionFuncList($userInfo['role_ids'], $userId);
            return $permissionList;
        }
    }
}