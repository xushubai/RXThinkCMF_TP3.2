<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP3.2混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Service;

/**
 * 基类服务
 * @author 牧羊人
 * @since 2021/1/17
 * Class BaseService
 */
class BaseService
{
    // 模型
    protected $model;

    /**
     * 获取数据列表
     * @return array
     * @since 2021/1/17
     * @author 牧羊人
     */
    public function getList()
    {
        // 初始化变量
        $map = array();
        $sort = 'id desc';
        $isSql = 0;

        // 获取参数
        $argList = func_get_args();
        if (!empty($argList)) {
            // 查询条件
            $map = (isset($argList[0]) && !empty($argList[0])) ? $argList[0] : array();
            // 排序
            $sort = (isset($argList[1]) && !empty($argList[1])) ? $argList[1] : 'id desc';
            // 是否打印SQL
            $isSql = isset($argList[2]) ? isset($argList[2]) : 0;
        }

        // 常规查询条件
        $param = I('post.', '', 'trim');
        if ($param) {
            // 筛选名称
            if (isset($param['name']) && $param['name']) {
                $map['name'] = array("like", "%{$param['name']}%");
            }
            // 筛选标题
            if (isset($param['title']) && $param['title']) {
                $map['title'] = array("like", "%{$param['title']}%");
            }
            // 筛选类型
            if (isset($param['type']) && $param['type']) {
                $map['type'] = $param['type'];
            }
            // 筛选状态
            if (isset($param['status']) && $param['status']) {
                $map['status'] = $param['status'];
            }
        }

        // 设置查询条件
        if (is_array($map)) {
            $map['mark'] = 1;
        } elseif ($map) {
            $map .= " AND mark=1 ";
        } else {
            $map .= " mark=1 ";
        }

        // 查询数据
        $result = $this->model->where($map)->order($sort)->page(PAGE, PERPAGE)->getField("id", true);

        // 打印SQL
        if ($isSql) {
            echo $this->model->_sql();
        }

        $list = array();
        if (is_array($result)) {
            foreach ($result as $val) {
                $info = $this->model->getInfo($val);
                $list[] = $info;
            }
        }

        //获取数据总数
        $count = $this->model->where($map)->count('id');

        //返回结果
        $result = array(
            "msg" => '操作成功',
            "code" => 0,
            "data" => $list,
            "count" => $count,
        );
        return $result;
    }

    /**
     * 添加或编辑
     * @return array
     * @since 2021/1/18
     * @author 牧羊人
     */
    public function edit()
    {
        // 获取参数
        $argList = func_get_args();
        // 查询条件
        $data = isset($argList[0]) ? $argList[0] : array();
        // 是否打印SQL
        $is_sql = isset($argList[1]) ? $argList[1] : false;
        // 未传值时默认获取值
        if (empty($data)) {
            $data = I('post.', '', 'trim');
        }
        $error = '';
        $result = $this->model->edit($data, $error, $is_sql);
        if ($result) {
            return message();
        }
        return message($error, false);
    }

    /**
     * 设置状态
     * @return array
     * @since 2021/1/18
     * @author 牧羊人
     */
    public function setStatus()
    {
        // 参数
        $data = I('post.', '', 'trim');
        // 记录ID
        if (!$data['id']) {
            return message('记录ID不能为空', false);
        }
        // 记录状态
        if (!$data['status']) {
            return message('记录状态不能为空', false);
        }
        $error = '';
        $result = $this->model->edit($data, $error);
        if (!$result) {
            return message($error, false);
        }
        return message();
    }

}