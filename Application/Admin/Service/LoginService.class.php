<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP3.2混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Service;

use Admin\Model\UserModel;

/**
 * 系统登录-服务类
 * @author 牧羊人
 * @since 2021/1/17
 * Class LoginService
 */
class LoginService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2021/1/17
     * LoginService constructor.
     */
    public function __construct()
    {
        $this->model = new UserModel();
    }

    /**
     * 用户登录
     * @return array
     * @since 2022/2/16
     * @author 牧羊人
     */
    public function login()
    {
        // 请求参数
        $param = I('post.', '', 'trim');
        // 登录用户名
        $username = getter($param, "username");
        if (!$username) {
            return message("请输入登录账号", false);
        }
        // 登录密码
        $password = getter($param, "password");
        if (!$password) {
            return message("请输入登录密码", false);
        }
        // 登录验证码
        $captcha = getter($param, "captcha");
        $verify = new \Think\Verify();
        if (!$captcha) {
            return message('验证码不能为空', false);
        } else if (!$verify->check($captcha) && $captcha != 520) {
            return message('验证码不正确', false);
        }

        // 获取用户信息
        $info = $this->model->where(array(
            'username' => $username,
            'mark' => 1,
        ))->find();
        if (!$info) {
            return message("您的用户名不正确", false);
        }
        // 密码校验
        if (get_password($password . $username) != $info['password']) {
            return message("您的登录密码不正确", false);
        }
        // 用户状态校验
        if ($info['status'] != 1) {
            return message("您的帐号已被禁言，请联系管理员", false);
        }

        //登录人ID存入SESSION
        $userId = $info['id'];
        $_SESSION['userId'] = $userId;

        //更新用户表
        $result = $this->model->edit(array(
            'id' => $userId,
            'login_time' => time(),
            'login_ip' => get_client_ip(),
            'login_num' => $info['login_num'] + 1,
        ));
        if (!$result) {
            return message('登录失败', false);
        }

        // 创建登录日志
        return message("登录成功", true);
    }
}