<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP3.2混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Model;


use Common\Model\BaseModel;

/**
 *
 * @author 牧羊人
 * @since 2021/3/14
 * Class UserPermissionModel
 * @package Admin\Model
 */
class UserPermissionModel extends BaseModel
{
    // 设置数据表
    protected $tableName = "user_permission";

    /**
     * 获取权限菜单列表
     * @param $roleIds 角色ID
     * @param $userId 用户ID
     * @param $type 类型
     * @param $pid 上级ID
     * @return mixed
     * @since 2022/2/17
     * @author 牧羊人
     */
    function getPermissionMenuList($roleIds, $userId, $type, $pid)
    {
        $map = [];
        if ($roleIds) {
            $map1 = [
                'up.type' => 1,
                'up.type_id' => array('in', $roleIds),
            ];
            $map[] = $map1;
        }
        $map2 = [
            'up.type' => 2,
            'up.type_id' => $userId,
        ];
        $map[] = $map2;
        // 或查询条件
        $map['_logic'] = 'OR';

        // 查询条件
        $where['m.type'] = array('in', [$type, $type + 1]);
        $where['m.pid'] = $pid;
        $where['m.status'] = 1;
        $where['m.mark'] = 1;
        $where['_complex'] = $map;
        // 查询数据
        $menuModel = new MenuModel();
        $menuList = $menuModel->alias('m')
            ->join(DB_PREFIX . 'user_permission as up ON up.menu_id=m.id')
//            ->distinct(true)
            ->where($where)
            ->order("pid asc,sort asc")
            ->field('m.*')
            ->select();
        if (!empty($menuList)) {
            foreach ($menuList as &$val) {
                if ($val['type'] >= 3) {
                    continue;
                }
                $childList = $this->getPermissionMenuList($roleIds, $userId, $val['type'], $val['id']);
                if (is_array($childList) && !empty($childList)) {
                    $val['children'] = $childList;
                }
            }
        }
        return $menuList;
    }

    /**
     * 获取权限节点列表
     * @param $roleIds 角色ID
     * @param $userId 用户ID
     * @return mixed
     * @since 2022/2/16
     * @author 牧羊人
     */
    function getPermissionFuncList($roleIds, $userId)
    {
        $map = [];
        if ($roleIds) {
            $map1 = [
                'up.type' => 1,
                'up.type_id' => array('in', $roleIds),
            ];
            $map[] = $map1;
        }
        $map2 = [
            'up.type' => 2,
            'up.type_id' => $userId,
        ];
        $map[] = $map2;
        // 或查询条件
        $map['_logic'] = 'OR';
        $where['m.type'] = 4;
        $where['m.status'] = 1;
        $where['m.mark'] = 1;
        $where['_complex'] = $map;
        $menuModel = new MenuModel();
        $funcList = $menuModel->alias('m')
            ->join(DB_PREFIX . 'user_permission as up ON up.menu_id=m.id')
//            ->distinct(true)
            ->where($where)
            ->order("pid asc,sort asc")
            ->field('m.permission')
            ->select();
        $funcList = array_key_value($funcList, "permission");
        return $funcList;
    }

}