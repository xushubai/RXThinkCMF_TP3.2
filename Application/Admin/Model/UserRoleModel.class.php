<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP3.2混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Model;


use Common\Model\BaseModel;

/**
 * 用户角色-模型
 * @author 牧羊人
 * @since 2021/1/17
 * Class UserRoleModel
 * @package Admin\Model
 */
class UserRoleModel extends BaseModel
{
    // 设置数据表
    protected $tableName = "user_role";

    /**
     * 根据用户ID获取角色列表
     * @param $userId 用户ID
     * @return array|false|mixed|string|null
     * @author 牧羊人
     * @since 2022/2/17
     */
    public function getUserRoleList($userId)
    {
        $roleList = $this->alias('ur')
            ->join(DB_PREFIX . 'role as r ON ur.role_id=r.id')
            ->where('ur.user_id=' . $userId)
            ->where('ur.user_id=' . $userId . ' and r.status=1 and r.mark=1')
            ->field('r.*')
            ->order('sort asc')
            ->select();
        return $roleList;
    }

}