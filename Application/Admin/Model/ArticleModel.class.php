<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP3.2混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Model;

use Common\Model\BaseModel;

/**
 * 文章-模型
 * @author 牧羊人
 * @since 2022/2/16
 */
class ArticleModel extends BaseModel
{
    // 设置数据表
    protected $tableName = 'article';

    /**
     * 获取记录信息
     * @param $id
     * @return \Common\Model\数据信息|mixed
     * @author 牧羊人
     * @since 2022/2/16
     */
    public function getInfo($id)
    {
        $info = parent::getInfo($id); // TODO: Change the autogenerated stub
        if ($info) {
            // 文章封面
            if ($info['cover']) {
                $info['cover_url'] = get_image_url($info['cover']);
            }

            // 获取栏目
            if ($info['cate_id']) {
                $itemCateModel = new ItemCateModel();
                $cateName = $itemCateModel->getCateName($info['cate_id'], ">>");
                $info['cate_name'] = $cateName;
            }

            // 获取分表
            $table = $this->getArticleTable($id, false);
            $articleModel = M($table);
            $articleInfo = $articleModel->find($id);
            if ($articleInfo['content']) {
                while (strstr($articleInfo['content'], "[IMG_URL]")) {
                    $articleInfo['content'] = str_replace("[IMG_URL]", IMG_URL, $articleInfo['content']);
                }
            }
            $info = array_merge($info, $articleInfo);

            // 文章图集
            if ($info['imgs']) {
                $imgsList = explode(',', $info['imgs']);
                foreach ($imgsList as &$val) {
                    $val = get_image_url($val);
                }
                $info['imgsList'] = $imgsList;
            }
        }
        return $info;
    }

    /**
     * 添加或编辑
     * @param $data 数据源
     * @param $error 错误信息
     * @param $is_sql 是否打印SQL
     * @return false|int|mixed|string
     * @author 牧羊人
     * @since 2022/2/17
     */
    public function edit($data, &$error = '', $is_sql = false)
    {
        // 获取数据表字段
        $column = $this->getFieldsList($this->name);
        $item = [];
        foreach ($data as $key => $val) {
            if (!in_array($key, array_keys($column))) {
                $item[$key] = $val;
                unset($data[$key]);
            }
        }

        // 开启事务
//        $this->startTrans();
        $rowId = parent::edit($data, $error, $is_sql);
        if (!$rowId) {
            //事务回滚
//            $this->rollback();
            return false;
        }
        $result = $this->saveArticleInfo($rowId, $item);
        if (!$result) {
            // 事务回滚
//            $this->rollback();
            return false;
        }
        // 提交事务
//        $this->commit();
        return $rowId;
    }

    /**
     * 保存文章附表信息
     * @param $id 记录ID
     * @param $item 数据源
     * @return bool
     * @since 2022/2/16
     * @author 牧羊人
     */
    public function saveArticleInfo($id, $item)
    {
        // 获取表名
        $table = $this->getArticleTable($id);
        $info = $this->where(['id' => $id])->table($table)->find();

        $data = [];
        if (!$info) {
            $data['id'] = $id;
        }
        // 文章内容
        $data['content'] = $item['content'];
        if ($item['guide']) {
            $data['guide'] = $item['guide'];
        }
        // 文章图集
        if ($item['imgs']) {
            $data['imgs'] = $item['imgs'];
        }

        //获取分表模型
        $table = $this->getArticleTable($id, false);
        $articleModel = M($table);
        if ($info['id']) {
            $result = $articleModel->where('id', $id)->save($data);
        } else {
            $result = $articleModel->add($data);
        }
        if ($result !== false) {
            return true;
        }
        return false;
    }

    /**
     * 获取文章附表名
     * @param $id 记录ID
     * @param bool $isPrefix 是否包含表前缀
     * @return string
     * @since 2020/7/4
     * @author 牧羊人
     */
    public function getArticleTable($id, $isPrefix = true)
    {
        $table = substr($id, -1, 1);
        $table = "article_{$table}";
        if ($isPrefix) {
            $table = DB_PREFIX . $table;
        }
        return $table;
    }

}